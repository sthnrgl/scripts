#!/bin/bash
systemctl --user restart plasma-plasmashell.service
# Old method (X Org):
#!/bin/bash
# echo "restart plasma"
# echo "kquitapp5 plasmashell"
# echo "kstart5 plasmashell"
# or
# kquitapp5 plasmashell &
# kstart5 plasmashell &
# plasmashell --replace &
