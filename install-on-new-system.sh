#!/bin/bash

# oh my zsh
#sudo zypper install -y zsh

# mix packages
#sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
sudo zypper install -y docker
sudo zypper install -y thefuck
sudo zypper install -y gparted
sudo zypper install -y btop
sudo zypper install -y fdupes
sudo zypper install -y okteta
sudo zypper install -y speedcrunch
sudo zypper install -y vim
sudo zypper install -y flac
sudo zypper install -y bc
sudo zypper install -y links
sudo zypper install -y mc
sudo zypper install -y tmux
sudo zypper install -y git
sudo zypper install -y neofetch
# sudo zypper install -y youtube-dl
# sudo zypper install -y youtube-dl-gui
# new fork:
sudo zypper install -y yt-dlp
#sudo zypper install -y youtube-dl-zsh-comletion
sudo zypper install -y opi
sudo zypper install -y kalarm
sudo zypper install -y steam
sudo zypper install -y patterns-glibc-hwcaps-x86_64_v3 fwupd-devel

# change KDE power management
sudo zypper remove -y tlp
sudo zypper install -y power-profiles-daemon
# java
#sudo zypper remove -y java-11-openjdk-devel
#sudo zypper install -y java-21-openjdk-devel
#sudo zypper install -y javapackages-tools

# flatpak
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
sudo flatpak install --noninteractive cc.arduino.IDE2
sudo flatpak install --noninteractive com.anydesk.Anydesk
sudo flatpak install --noninteractive com.discordapp.Discord
sudo flatpak install --noninteractive com.valvesoftware.SteamLink
sudo flatpak install --noninteractive md.obsidian.Obsidian
# sudo flatpak install --noninteractive org.audacityteam.Audacity
sudo flatpak install --noninteractive org.clementine_player.Clementine
# sudo flatpak install --noninteractive org.kde.krita
# sudo flatpak install --noninteractive org.pipewire.Helvum
sudo flatpak install --noninteractive org.telegram.desktop
sudo flatpak install --noninteractive us.zoom.Zoom
# sudo flatpak install --noninteractive com.calibre_ebook.calibre
sudo flatpak install --noninteractive com.github.AlizaMedicalImaging.AlizaMS
sudo flatpak install --noninteractive com.github.jeromerobert.pdfarranger
# sudo flatpak install --noninteractive com.github.wwmm.easyeffects
# sudo flatpak install --noninteractive com.github.xournalpp.xournalpp

# codecs
# opi codecs
