#!/bin/bash
# read qr from screenshot
# Set the directory path
directory=~/Pictures

# Find the latest .png file starting with "Screenshot"
latest_file=$(find "$directory" -type f -name 'Screenshot*.png' -printf '%T@ %p\n' | sort -n | tail -1 | awk '{print $2}')

# Check if a file was found
if [ -n "$latest_file" ]; then
    echo "Latest screenshot file: $latest_file"
else
    echo "No screenshot file found."
fi
qrencode -r $latest_file -t UTF8
