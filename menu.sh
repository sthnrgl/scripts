#!/bin/bash

# Function to display the menu
display_menu() {
    clear
    echo "Select a script to execute:"
    index=1
    for script in "${scripts[@]}"; do
        echo "$index. $script"
        ((index++))
    done
    echo "0. Exit"
}

# Function to execute the selected script
execute_script() {
    local selected_script="${scripts[$1 - 1]}"
    if [ -x "$selected_script" ]; then
        ./"$selected_script"
    else
        echo "Error: $selected_script is not executable."
    fi
}

Main script
echo "Enter the directory path where the scripts are located:"
directory=$1

if [ ! -d "$directory" ]; then
    directory="."
fi


# Find executable scripts in the specified directory
scripts=($(find "$directory" -type f -name "*.sh" -executable -printf "%f\n"))

if [ ${#scripts[@]} -eq 0 ]; then
    echo "Error: No executable scripts found in '$directory'."
    exit 1
fi

# while true; do
    display_menu

    read -p "Enter your choice (0 to exit): " choice

    if [[ $choice =~ ^[0-9]+$ ]]; then
        if (( choice == 0 )); then
            echo "Exiting..."
            exit 0
        elif (( choice > 0 && choice <= ${#scripts[@]} )); then
            execute_script "$choice"
            # read -p "Press Enter to return to the menu."
        else
            echo "Error: Invalid choice. Please enter a number between 0 and ${#scripts[@]}."
            read -p "Press Enter to continue."
        fi
    else
        echo "Error: Please enter a valid number."
        read -p "Press Enter to continue."
    fi
# done
