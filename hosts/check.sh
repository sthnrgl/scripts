#!/bin/bash

# Specify the path to your text file
file_path="hosts"

# Check if the file exists
if [ -e "$file_path" ]; then
    # Use grep to check if any line does not begin with "0.0.0.0"
    if grep -q -v "^0.0.0.0" "$file_path"; then
        echo "Lines that do not match the pattern:"
        grep -v "^0.0.0.0" "$file_path"
    else
        echo "OK"
    fi
else
    echo "File not found: $file_path"
fi
