#!/bin/bash
echo "curl wttr.in/kyiv"
curl wttr.in/kyiv
# To specify your own custom output format, use the special %-notation:
#
#     c    Weather condition,
#     C    Weather condition textual name,
#     x    Weather condition, plain-text symbol,
#     h    Humidity,
#     t    Temperature (Actual),
#     f    Temperature (Feels Like),
#     w    Wind,
#     l    Location,
#     m    Moon phase 🌑🌒🌓🌔🌕🌖🌗🌘,
#     M    Moon day,
#     p    Precipitation (mm/3 hours),
#     P    Pressure (hPa),
#     u    UV index (1-12),
#
#     D    Dawn*,
#     S    Sunrise*,
#     z    Zenith*,
#     s    Sunset*,
#     d    Dusk*,
#     T    Current time*,
#     Z    Local timezone.
#
# (*times are shown in the local timezone)
# curl 'wttr.in/Kyiv?format="%l:+%c+%t+%w+%P+%h+UV:%u\n"'
curl v2.wttr.in/Kyiv
#
# So, these two calls are the same:
#
#     $ curl wttr.in/London?format=3
#     London: ⛅️ +7⁰C
#     $ curl wttr.in/London?format="%l:+%c+%t\n"
#     London: ⛅️ +7⁰C
