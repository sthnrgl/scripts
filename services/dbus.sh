
# virtual keyboard on/off
qdbus6 --session org.kde.KWin /VirtualKeyboard enabled true
qdbus6 --session org.kde.KWin /VirtualKeyboard enabled false
# or
qdbus6 --session org.kde.keyboard /VirtualKeyboard  org.kde.kwin.VirtualKeyboard.enabled true
# tablet mode monitoring
qdbus6 --session org.kde.KWin /org/kde/KWin org.kde.KWin.TabletModeManager.tabletMode
# or
dbus-monitor --session "sender=org.kde.KWin, path=/org/kde/KWin, interface=org.kde.KWin.TabletModeManager, member=tabletModeChanged"

dbus-monitor --session "sender=org.kde.KWin, path=/org/kde/KWin/NightLight, interface=org.kde.KWin.NightLight, member=inhibited"
