#!/bin/bash
# This script enables or disables the virtual keyboard depending on whether the tablet mode is active or not
# Function to handle the output of the DBus service
handle_output() {
    if [ "$1" == "true" ]; then
        qdbus6 --session org.kde.KWin /VirtualKeyboard enabled true
    elif [ "$1" == "false" ]; then
        qdbus6 --session org.kde.KWin /VirtualKeyboard enabled false
    fi
}

# Continuously monitor the DBus service output
while true; do

    dbus-monitor --session "sender=org.kde.KWin, path=/org/kde/KWin, interface=org.kde.KWin.TabletModeManager, member=tabletModeChanged" | \
    while read -r line; do
        if echo "$line" | grep -q 'true'; then
            handle_output "true"
        elif echo "$line" | grep -q 'false'; then
            handle_output "false"
        fi
    done
done
