#!/bin/bash
# Record the start time
start_time=$(date +%s)

txt_files=$(find . -maxdepth 1 -type f -name "*.txt")
mapfile -t array < "patterns"

# Delete the last character from each array element
# for i in "${!array[@]}"; do
#     array[$i]=${array[$i]%?}
# done

for pattern in "${array[@]}"; do
    echo "--------------------------------"
    echo "$pattern"
    echo "--------------------------------"
    all=0
    for file in $txt_files; do
        result=$(grep -o "$pattern" "$file" | wc -l)
        echo "$file --> $result"
        all=$((all + result))
    done
    echo "ALL: $all"
done

# Record the end time
end_time=$(date +%s)
# Calculate the elapsed time
elapsed_time=$((end_time - start_time))
# Print the elapsed time
echo "Elapsed time: $elapsed_time seconds"



