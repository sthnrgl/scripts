#!/bin/bash
df -h
# Cleanup journal logs
# You can safely clean up journal logs and keep only logs from yesterday: 
sudo journalctl --vacuum-time=1d

# This command will delete the old kernels based on configuration at /etc/zypp/zypp.conf 
sudo zypper purge-kernels

# Tell snapper to delete old snapshots based on configuration at /etc/snapper/configs/root 
sudo snapper cleanup number
# You can reduce it using the following command: 
# snapper set-config SPACE_LIMIT=0.2 NUMBER_LIMIT=2-6 NUMBER_LIMIT_IMPORTANT=4

# Cleanup /tmp
sudo rm /tmp/* -rf

df -h

